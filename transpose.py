#!/usr/bin/env python3
import sys
import re

import pandas as pd

data = pd.read_csv(sys.argv[1], sep = '\t')
output = data.transpose().to_string(header = False)

print(re.sub(r' +', r'\t', output))