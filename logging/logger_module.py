#!/usr/bin/env python3
import logging

log_levels = {
    1: logging.DEBUG,
    2: logging.INFO,
    3: logging.WARNING,
    4: logging.ERROR,
    5: logging.CRITICAL
}

def make_logger(name, 
                filename, 
                file_log_level = 1, 
                stream_log_level = 1):
    file_log_format = logging.Formatter('%(name)s[%(levelname)s][%(asctime)s]: %(message)s')
    stream_log_format = logging.Formatter('%(name)s[%(levelname)s]: %(message)s')
    
    logger = logging.getLogger(name)
    logger.setLevel(log_levels[1])

    filehandler = logging.FileHandler(filename, mode='w')
    filehandler.setFormatter(file_log_format)
    filehandler.setLevel(log_levels[file_log_level])

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(stream_log_format)
    stream_handler.setLevel(log_levels[stream_log_level])

    logger.addHandler(filehandler)
    logger.addHandler(stream_handler)

    return logger
