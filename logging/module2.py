#!/usr/bin/env python3

from logger_module import make_logger

logger = make_logger(__name__, 'm2.log')

def module2_function():
    logger.debug('hello')
    