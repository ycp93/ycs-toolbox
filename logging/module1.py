#!/usr/bin/env python3

from logger_module import make_logger

logger = make_logger(__name__, 'm1.log', file_log_level=3)

def module1_function():
    logger.debug('Debug message')
    logger.warning('hello')
