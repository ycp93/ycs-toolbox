#!/usr/bin/env python3

import sys
import json
import requests

import click

@click.group()
def cli():
    """ YC's Ensembl query script """

@cli.command()
@click.argument('rsid', nargs=-1)
def query(rsid):
    server = "https://grch37.rest.ensembl.org"

    if len(rsid)==1:
        ext = "/variation/human/{rsid[0]}?"
        r = requests.get(server+ext, headers={ "Content-Type" : "application/json"})
    if len(rsid)>1:
        ext = "/variation/homo_sapiens"
        headers={ "Content-Type" : "application/json", "Accept" : "application/json"}
        data = json.dumps({'ids': rsid})
        r = requests.post(server+ext, headers=headers, data=data)

    if not r.ok:
        r.raise_for_status()
        sys.exit()
 
    decoded = r.json()

    if len(rsid)==1:
        mappings = decoded['mappings'][0]

        chrom = mappings['seq_region_name']
        start = mappings['start']
        end = mappings['end']
        alleles = mappings['allele_string']
        var_class = decoded['var_class']
        conseq = decoded['most_severe_consequence']

        click.echo(f'{rsid}\t{chrom}\t{start}\t{end}\t{alleles}\t{var_class}\t{conseq}')
    else:
        for k, v in decoded.items():
            mappings = v['mappings'][0]
            chrom = mappings['seq_region_name']
            start = mappings['start']
            end = mappings['end']
            alleles = mappings['allele_string']
            var_class = v['var_class']
            conseq = v['most_severe_consequence']

            click.echo(f'{k}\t{chrom}\t{start}\t{end}\t{alleles}\t{var_class}\t{conseq}')
  

if __name__ == '__main__':
    cli()
