#!/usr/bin/env python3
import os
import shutil
import doctest

class Dependencies:
    ''' A class to define and checks the dependencies and of the pipeline '''

    def __init__(self, exes = None, envs = None):
        """
        >>> deps = Dependencies(exes = ['cd'], envs = ['PATH'])
        """
        self.exes = exes
        self.envs = envs

    def __repr__(self):
        exe_keys = [k for k in self.exes.keys()]
        env_keys = [k for k in self.envs.keys()]
        return f"Dependencies(exes = {exe_keys}, envs = {env_keys})"

    @property
    def exes(self):
        return self._exes
    
    @exes.setter
    def exes(self, exes: list):
        self._exes = self.get_exes(*exes)
        self.missing_exes = self.none_value(self.exes)
        
    @property
    def envs(self):
        return self._envs
    
    @envs.setter
    def envs(self, envs: list):
        self._envs = self.get_envs(*envs)
        self.missing_envs = self.none_value(self.envs)

    @staticmethod
    def get_exes(*args):
        """
        Checks if executable file is in PATH
        >>> Dependencies.get_exes('cd', 'nonexistant_exe')
        {'cd': '/usr/bin/cd', 'nonexistant_exe': None}
        """
        # TODO: The above test is not reliable. 
        # Find a reliable way to test this method
        return {exe: shutil.which(exe) for exe in args}
        
    @staticmethod
    def get_envs(*args):
        """
        Checks and gets environment variable values
        >>> import os
        >>> os.environ['MYENV'] = 'hello'
        >>> Dependencies.get_envs('MYENV', 'empty_env')
        {'MYENV': 'hello', 'empty_env': None}
        """
        return {env: os.environ.get(env) for env in args}

    @staticmethod
    def none_value(dict):
        """
        Returns a tuple of keys which have None value
        >>> Dependencies.none_value({'key1': 'string', 'key2': None})
        ('key2',)
        """
        return tuple(key for key, value in dict.items() if value is None) 

doctest.testmod()



